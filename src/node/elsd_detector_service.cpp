#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iomanip>
#include <cstdint>
#include <boost/thread/thread.hpp>
#include <chrono>

//ROS
#include <ros/ros.h>
#include <geometry_msgs/Point.h>

//OPENCV
#include <opencv2/opencv.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/features2d.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

//Boost
#include <boost/algorithm/string.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

// Elsd
#include <Elsd.hpp>

//Polynomial Detection
#include <vision_pkg/PolyDetectionService.h>

ros::NodeHandle *nh;

Elsd elsd;
double max_radius = 30;

bool integration_service_callback(vision_pkg::PolyDetectionService::Request &req, vision_pkg::PolyDetectionService::Response &res)
{
    ROS_INFO("detecting...");
    cv::Mat img;
    if (req.colored)
    {
        img = cv_bridge::toCvCopy(req.image, "bgr8")->image;
    }
    else
    {
        img = cv_bridge::toCvCopy(req.image, "8UC1")->image;
    }

    std::vector<Ring> rings;
    std::vector<Polygon> polygons;
    elsd.computeRings(img, rings, polygons, req.colored);

    for (int i = 0; i < rings.size(); i++)
    {

        Ring &r = rings[i];
        if (req.circle_max_radius > 0)
            if (r.ax > req.circle_max_radius || r.bx > req.circle_max_radius)
                continue;

        geometry_msgs::Point32 circle;
        circle.x = r.cx;
        circle.y = r.cy;
        circle.z = (r.ax + r.bx) / 2.0;
        res.circles.push_back(circle);
    }

    ROS_INFO("\t\t...done!");
    return true;
}

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char **argv)
{

    // Initialize ROS
    ros::init(argc, argv, "elsd_detector_service");
    nh = new ros::NodeHandle("~");

    //Services
    ros::ServiceServer service_integration = nh->advertiseService("detection_service", integration_service_callback);

    int hz;
    nh->param<int>("hz", hz, 30);

    ros::spin();
}