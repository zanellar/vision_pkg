#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"

#include <image_transport/image_transport.h>
#include <opencv2/opencv.hpp>

#include <cv_bridge/cv_bridge.h>

using namespace std;

cv::Mat img;

void imageCallback(const sensor_msgs::ImageConstPtr &msg)
{
    img = cv_bridge::toCvShare(msg, "bgr8")->image;
}

/**
 *
 */

double hz = 30;

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "image_listener_2");
    ros::NodeHandle node;
    cv::namedWindow("view");
    cv::startWindowThread();

    image_transport::ImageTransport it(node);
    image_transport::Subscriber sub = it.subscribe("/usb_cam_1/image_raw", 1, imageCallback);

    ros::Rate r(hz);
    while (node.ok())
    {
        cout << "Hello World!\n\n";

        cv::imshow("view", img);
        cv::waitKey(1);

        cv::destroyWindow("view");

        r.sleep();
        ros::spinOnce();
    }

    return 0;
}