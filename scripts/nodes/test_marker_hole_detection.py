#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import numpy as np
import PyKDL
import superros.transformations as transformations
from superros.logger import Logger
from superros.comm import RosNode

# Vision
import cv2
from visionpylib.cameras import CameraHandler
from visionpylib.markers import MarkerDetector
from visionpylib.image import ImageAreaOfInterest, ImageProcessing
import visionpylib.visionutils as visionutils

# matching matching
from visionpylib.matching import FeatureMatching


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class MarkerHandler(object):
    def __init__(self, camera):
        self.camera = camera
        self.image = None
        self.reference_marker = None
        self.pixel_ratio = 0
        self.marker_tf_2d = None

        # Area of interest
        self.image_area = ImageAreaOfInterest()

        # Creates marker detector
        self.marker_detector = MarkerDetector(camera_file=self.camera.getCameraFile(), z_up=True)

    def detect(self, image, markers_map, camera_tf_name=None):
        ''' Detects the markers on the image.
        \nIt recieves a map with the marker 'id' as 'key' and the marker 'size' as 'value'
        \nIt returns a map with the marker 'id' as 'key' and the marker 'tf' as 'value'. '''
        self.image = image
        if image is None:
            return

        markers = self.marker_detector.detectMarkersMap(self.image, markers_map=markers_map)

        self.reference_marker = None
        for id, marker in markers.iteritems():
            # print("MARKER", marker)
            reference_size = markers_map[id]

            # Publish marker tf
            if camera_tf_name:
                node.broadcastTransform(marker, marker.getName(),
                                        camera_tf_name, node.getCurrentTime())

            self.reference_marker = marker
            break  # we take only the first one detected

        if self.reference_marker:
            self.pixel_ratio = self.reference_marker.measurePixelRatio(reference_size)  # compute meters/pixes ratio
            self.marker_tf_2d = self.reference_marker.get2DFrame()  # 2D tf of the marker (tf on the marker's plane)

    def isDetected(self):
        return self.reference_marker is not None

    def draw(self):
        ''' Draw the marker bourders and its reference frame on the source image'''
        if self.reference_marker:
            self.reference_marker.draw(image=self.image, color=np.array([0, 0, 255]), scale=1, draw_center=True)
            visionutils.draw2DReferenceFrame(self.marker_tf_2d, self.image)

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    cv2.namedWindow("holes", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('holes', 1000, 800)
    cv2.namedWindow("output", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('output', 1000, 800)

    # camera parameters
    node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw/compressed")  # /camera/rgb/image_raw/compressed
    node.setupParameter("CAMERA_CONFIGURATION", 'microsoft_live_camera_focus5.yml')  # asus_camera_1_may2017.yml
    node.setupParameter("CAMERA_TF_NAME", '/comau_smart_six/tool')

    # marker parameters
    node.setupParameter("MARKER_ID", 705)
    node.setupParameter("MARKER_SIZE", 0.01)

    # variables
    camera_tf_name = node.getParameter("hz")
    camera_topic = node.getParameter("CAMERA_TOPIC")
    camera_calibration_file = node.getParameter("CAMERA_CONFIGURATION")
    camera_calibration_file_path = node.getFileInPackage('vision_pkg', 'data/camera_calibration/' + camera_calibration_file)
    markers_map = {node.getParameter("MARKER_ID"): node.getParameter("MARKER_SIZE")}

    #⬢⬢⬢⬢⬢➤ CAMERA
    camera = CameraHandler(camera_calibration_file_path, camera_topic)
    #⬢⬢⬢⬢⬢➤ MARKER
    marker = MarkerHandler(camera.getCamera())

    #⬢⬢⬢⬢⬢➤ Circle Detection Object
    feature = FeatureMatching()
    # feature.setELSDService(node)

    #▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇

    while node.isActive():
        image = camera.getImage()

        if image is not None:
            # detect the markers on the image
            marker.detect(image=image,
                          markers_map=markers_map,
                          camera_tf_name="world")

            #⬢⬢⬢⬢⬢➤ Image Area of Interest
            image_area = ImageAreaOfInterest()
            relative_tf_2d = PyKDL.Frame(PyKDL.Vector(70, 40, 0))
            image_area.update(image, marker.marker_tf_2d, relative_tf_2d)
            image_area.setArea(height=130, width=70)
            masked_image = image_area.getMaskedImage(show_it=False)

            if marker.isDetected():
                #⬢⬢⬢⬢⬢➤ Image Processing
                # image_proc = image
                image_proc = ImageProcessing.grayScale(masked_image, filter_it=False)
                # image_proc = ImageProcessing.resizeImage(image_proc, (480, 640))

                #⬢⬢⬢⬢⬢➤ Detect Circles
                # Hough method
                holes = feature.detectCircle(image_proc, min_radius=15, max_radius=25, method=feature.HOUGH)
                print holes
                # cv2.imwrite('/home/riccardo/Pictures/vision/test_template/output.png', image_proc)

                #⬢⬢⬢⬢⬢➤ Augmented reality on image
                image_area.draw()
                marker.draw()

            cv2.imshow("holes", image_proc)
            cv2.imshow("output", image)
            c = cv2.waitKey(1)

        node.tick()
