#!/usr/bin/env python
# -*- encoding: utf-8 -*-


''' Example of marker detection with ArUco from camera live image. 
 * Broadcast the 3D marker-frame.
 * Define the area of interest w.r.t. the marker.
 * Draw the marker bourders, its 2D frame and the respective area of interest on the image.
 * Do some image processing on the area of interest
 '''

import rospy
import math
import numpy as np
import PyKDL

from superros.logger import Logger
from superros.comm import RosNode
import superros.transformations as transformations

import cv2
from cv2 import aruco
from visionpylib.cameras import CameraRGB
from visionpylib.my_markers import MarkerDetector
from visionpylib.image import ImageAreaOfInterest, ImageProcessing
import visionpylib.visionutils as visionutils

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class CameraHandler(object):                                
    def __init__(self, calibration_file, camera_topic):         

        # Create sCamera Proxy
        self.camera = CameraRGB(
            configuration_file=calibration_file,
            rgb_topic=camera_topic,
            compressed_image='compressed' in camera_topic
        )

        # Camera Msgs Callback
        self.camera.registerUserCallabck(self.camera_callback)
        self.frame = None
        self.image = None

    def camera_callback(self, frame_msg):
        self.frame = frame_msg
        self.image = self.frame.rgb_image.copy()

    def getCamera(self):
        return self.camera

    def getFrame(self):
        return self.frame

    def getImage(self):
        return self.image


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class MarkerHandler(object):
    def __init__(self, camera):
        self.camera = camera
        self.image = None
        self.reference_marker = None
        self.pixel_ratio = 0
        self.marker_tf_2d = None

        # Area of interest
        self.image_area = ImageAreaOfInterest()

        # Creates marker detector
        self.marker_detector = MarkerDetector(camera_file=self.camera.getCameraFile(), z_up=True) 

    def detect(self, image, markers_map, camera_tf_name=None):
        ''' Detects the markers on the image.
        \nIt recieves a map with the marker 'id' as 'key' and the marker 'size' as 'value'
        \nIt returns a map with the marker 'id' as 'key' and the marker 'tf' as 'value'. '''
        self.image = image
        if image is None:
            return

        markers = self.marker_detector.detectMarkersMap(self.image, markers_map=markers_map)

        self.reference_marker = None
        for id, marker in markers.iteritems():      #Poaible error
            # print("MARKER", marker)
            reference_size = markers_map[id]

            # Publish marker tf
            if camera_tf_name:
                node.broadcastTransform(marker, marker.getName(),
                                        camera_tf_name, node.getCurrentTime())

            self.reference_marker = marker
            break  # we take only the first one detected

        if self.reference_marker:
            self.pixel_ratio = self.reference_marker.measurePixelRatio(reference_size)  # compute meters/pixels ratio
            self.marker_tf_2d = self.reference_marker.get2DFrame()  # 2D tf of the marker (tf on the marker's plane)

    def isDetected(self):
        return self.reference_marker is not None

    def draw(self):
        ''' Draw the marker bourders and its reference frame on the source image'''
        if self.reference_marker:
            self.reference_marker.draw(image=self.image, color=np.array([0, 0, 255]), scale=1, draw_center=True)
            visionutils.draw2DReferenceFrame(self.marker_tf_2d, self.image)


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 15)  #
    node.setHz(node.getParameter("hz"))

    # camera parameters
    node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw/compressed")  # /camera/rgb/image_raw/compressed
    node.setupParameter("CAMERA_CONFIGURATION", 'microsoft_live_camera_focus5.yml')  # asus_camera_1_may2017.yml
    node.setupParameter("CAMERA_TF_NAME", 'fixed_camera')  # camera tf (by default is the "fixed_camera", see "camers.launch")

    # marker parameters
    node.setupParameter("MARKER_ID", 42)
    node.setupParameter("MARKER_SIZE", 0.01)

    #⬢⬢⬢⬢⬢➤ VARIABLES
    camera_tf_name = node.getParameter("CAMERA_TF_NAME")
    camera_topic = node.getParameter("CAMERA_TOPIC")
    camera_calibration_file = node.getParameter("CAMERA_CONFIGURATION")
    camera_calibration_file_path = node.getFileInPackage('vision_pkg', 'data/camera_calibration/' + camera_calibration_file)
    markers_map = {node.getParameter("MARKER_ID"): node.getParameter("MARKER_SIZE")} # markers_map ={"703":0.01, "81":0.01}

    #⬢⬢⬢⬢⬢➤ CAMERA
    camera = CameraHandler(camera_calibration_file_path, camera_topic)

    #⬢⬢⬢⬢⬢➤ MARKER
    marker = MarkerHandler(camera.getCamera())

    show_it = True

    #▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇

    while node.isActive():
        image = camera.getImage()
        if image is not None:
            original_image_copy = image.copy()

            # detect the markers on the image
            marker.detect(image=image,
                          markers_map=markers_map,
                          camera_tf_name=camera_tf_name)

            if marker.isDetected():

                #⬢⬢⬢⬢⬢➤ Image Area of Interest
                image_area = ImageAreaOfInterest()
                relative_tf_2d = PyKDL.Frame(PyKDL.Vector(130, 0, 0))
                image_area.update(image, marker.marker_tf_2d, relative_tf_2d)
                image_area.setArea(height=100, width=300)

                masked_image = image_area.getMaskedImage(show_it=False)
                cropped_image = image_area.getCroppedArea(show_it=False)

                #⬢⬢⬢⬢⬢➤ Image Processing
                # image_proc = cropped_image
                # image_proc = ImageProcessing.grayScale(image_proc, filter_it=True, show_it=False)
                # image_proc = ImageProcessing.adaptiveThreshold(image_proc, show_it=False)
                # image_proc = ImageProcessing.saltAndPepper(image_proc, kerner_size=3, show_it=False)
                # image_proc = ImageProcessing.fixedThreshold(image_proc, th=200, show_it=True)
                # image_proc = ImageProcessing.templateMatch(image_proc, show_it=True)

                #⬢⬢⬢⬢⬢➤ Augmented reality on image
                image_area.draw()
                marker.draw()

            if show_it:
                cv2.namedWindow("output", cv2.WINDOW_NORMAL)
                cv2.imshow("output", image)

            c = cv2.waitKey(1)

        node.tick()
        