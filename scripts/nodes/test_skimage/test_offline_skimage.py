#!/usr/bin/env python
# -*- encoding: utf-8 -*-


''' Template matching of a pre-loaded image'''


import rospy
import math
import numpy as np
import PyKDL
import superros.transformations as transformations
from superros.logger import Logger
from superros.comm import RosNode

# Vision
import cv2
from visionpylib.image import ImageProcessing
import visionpylib.visionutils as visionutils

# matching matching
from visionpylib.matching import TemplateMatching

# skimage
from skimage import data
from skimage.feature import match_template

from matplotlib import pyplot as plt


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    #⬢⬢⬢⬢⬢➤ Load Image
    image_template_name = "/home/riccardo/Pictures/vision/test_template/template_component_705.png"
    image_scene_name = "/home/riccardo/Pictures/vision/test_template/component_705.jpg"

    image_template = cv2.imread(image_template_name, cv2.IMREAD_COLOR)
    image_scene = cv2.imread(image_scene_name, cv2.IMREAD_COLOR)
    # image_original = image_scene.copy()

    #⬢⬢⬢⬢⬢➤ Image Processing
    image_template = ImageProcessing.grayScale(image_template, filter_it=False, show_it=False)
    image_template = ImageProcessing.resizeImage(image_template, (480, 640))
    image_scene = ImageProcessing.grayScale(image_scene, filter_it=False, show_it=False)
    image_scene = ImageProcessing.resizeImage(image_scene, (480, 640))

    #⬢⬢⬢⬢⬢➤ Find the Object
    result = match_template(image_scene, image_template)
    ij = np.unravel_index(np.argmax(result), result.shape)
    x, y = ij[::-1]
    print (x, y)

    cv2.namedWindow("output", cv2.WINDOW_NORMAL)
    cv2.circle(image_scene, (x, y), 3, (0, 0, 255), 5)

    while node.isActive():
        cv2.imshow("output", image_scene)
        c = cv2.waitKey(1)

        node.tick()

    # fig = plt.figure(figsize=(8, 3))
    # ax1 = plt.subplot(1, 3, 1)
    # ax2 = plt.subplot(1, 3, 2)
    # ax3 = plt.subplot(1, 3, 3)

    # ax1.imshow(image_template, cmap=plt.cm.gray)
    # ax1.set_axis_off()
    # ax1.set_title('template')

    # ax2.imshow(image_scene, cmap=plt.cm.gray)
    # ax2.set_axis_off()
    # ax2.set_title('scene')

    # # highlight matched region
    # rect_height, rect_width = image_template.shape
    # rect = plt.Rectangle((x, y), rect_width, rect_height, edgecolor='r', facecolor='none')
    # ax2.add_patch(rect)

    # ax3.imshow(result)
    # ax3.set_axis_off()
    # ax3.set_title('`match_template`\nresult')

    # # highlight matched region
    # ax3.autoscale(False)
    # ax3.plot(x, y, 'o', markeredgecolor='r', markerfacecolor='none', markersize=10)

    # plt.show()
