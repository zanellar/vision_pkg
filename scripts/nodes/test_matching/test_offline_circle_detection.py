#!/usr/bin/env python
# -*- encoding: utf-8 -*-

''' Circle (hole) detection of a pre-loaded image using ELSD and Hough methods'''

import rospy
import math
import numpy as np
import PyKDL
import superros.transformations as transformations
from superros.logger import Logger
from superros.comm import RosNode

# Vision
import cv2
from visionpylib.image import ImageProcessing
import visionpylib.visionutils as visionutils

# matching matching
from visionpylib.matching import FeatureMatching


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    # cv2.namedWindow("output", cv2.WINDOW_NORMAL)

    #⬢⬢⬢⬢⬢➤ Circle Detection Object
    feature = FeatureMatching()
    feature.setELSDService(node)

    #⬢⬢⬢⬢⬢➤ Load Image
    test_image_folder = "/home/riccardo/Pictures/vision/test_hole/"
    img_file_name = "hole1"
    image = cv2.imread(test_image_folder + img_file_name + '.jpg', cv2.IMREAD_COLOR)

    #⬢⬢⬢⬢⬢➤ Image Processing
    image_proc = image
    image_proc = ImageProcessing.grayScale(image_proc, filter_it=True, show_it=False)
    image_proc = ImageProcessing.resizeImage(image_proc, (480, 640))

    #⬢⬢⬢⬢⬢➤ Detect Circles
    # Hough method
    im1 = image_proc.copy()
    feature.detectCircle(im1, min_radius=1, max_radius=15, method=feature.HOUGH, show_it=False)
    cv2.imwrite('/home/riccardo/Pictures/vision/test_hole/output/' + img_file_name + '_HOUGH.png', im1)

    # ELSD method
    im2 = image_proc.copy()
    feature.detectCircle(im2, min_radius=1, max_radius=15, method=feature.ELSD, show_it=True)
    cv2.imwrite('/home/riccardo/Pictures/vision/test_hole/output/' + img_file_name + '_ELSD.png', im2)

    while node.isActive():
        c = cv2.waitKey(1)
        node.tick()
