#!/usr/bin/env python
# -*- encoding: utf-8 -*-

''' Circle (hole) detection of a live camera image using Hough methods'''

import rospy
import math
import numpy as np
import PyKDL
import superros.transformations as transformations
from superros.logger import Logger
from superros.comm import RosNode

# Vision
import cv2
from visionpylib.cameras import CameraHandler
from visionpylib.image import ImageAreaOfInterest, ImageProcessing
import visionpylib.visionutils as visionutils

# matching matching
from visionpylib.matching import FeatureMatching


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 500)  #
    node.setHz(node.getParameter("hz"))

    # camera parameters
    node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw/compressed")  # /camera/rgb/image_raw/compressed
    node.setupParameter("CAMERA_CONFIGURATION", 'microsoft_live_camera_focus5.yml')  # asus_camera_1_may2017.yml
    node.setupParameter("CAMERA_TF_NAME", '/comau_smart_six/tool')

    #⬢⬢⬢⬢⬢➤ Camera
    camera_tf_name = node.getParameter("hz")
    camera_topic = node.getParameter("CAMERA_TOPIC")
    camera_calibration_file = node.getParameter("CAMERA_CONFIGURATION")
    camera_calibration_file_path = node.getFileInPackage('vision_pkg', 'data/camera_calibration/' + camera_calibration_file)

    # camera object
    camera = CameraHandler(camera_calibration_file_path, camera_topic)

    #⬢⬢⬢⬢⬢➤ Circle Detection Object
    feature = FeatureMatching()
    # feature.setELSDService(node)

    show_it = True
    cv2.namedWindow("output", cv2.WINDOW_NORMAL)

    #▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇

    while node.isActive():
        image = camera.getImage()
        if image is not None:
            original_image_copy = image.copy()

            #⬢⬢⬢⬢⬢➤ Image Processing
            image_proc = image
            image_proc = ImageProcessing.grayScale(image_proc, filter_it=True, show_it=False)
            #⬢⬢⬢⬢⬢➤ Detect Circles
            circles_detected = feature.detectCircle(image_proc, min_radius=5, max_radius=40, method=feature.HOUGH, show_it=True)
            print circles_detected
            cv2.imshow("output", image_proc)
            c = cv2.waitKey(1)
        else:
            Logger.error("None Image")

        node.tick()
