#!/usr/bin/env python
# -*- encoding: utf-8 -*-


''' Template matching on a live camera image'''

import rospy
import math
import numpy as np
import PyKDL
import superros.transformations as transformations
from superros.logger import Logger
from superros.comm import RosNode

# Vision
import cv2
from visionpylib.cameras import CameraHandler
from visionpylib.image import ImageProcessing
import visionpylib.visionutils as visionutils

# matching matching
from visionpylib.matching import TemplateMatching

from matplotlib import pyplot as plt


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    # camera parameters
    node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw/compressed")  # /camera/rgb/image_raw/compressed
    node.setupParameter("CAMERA_CONFIGURATION", 'microsoft_live_camera_focus5.yml')  # asus_camera_1_may2017.yml
    node.setupParameter("CAMERA_TF_NAME", '/comau_smart_six/tool')

    #⬢⬢⬢⬢⬢➤ Camera
    camera_tf_name = node.getParameter("hz")
    camera_topic = node.getParameter("CAMERA_TOPIC")
    camera_calibration_file = node.getParameter("CAMERA_CONFIGURATION")
    camera_calibration_file_path = node.getFileInPackage('vision_pkg', 'data/camera_calibration/' + camera_calibration_file)

    # camera object
    camera = CameraHandler(camera_calibration_file_path, camera_topic)

    #⬢⬢⬢⬢⬢➤ Load Image
    image_template_name = "/home/riccardo/Pictures/vision/test_template/template_component2.jpg"
    image_template = cv2.imread(image_template_name, cv2.IMREAD_COLOR)

    #⬢⬢⬢⬢⬢➤ Image Processing
    # image_template = ImageProcessing.grayScale(image_template, filter_it=False, show_it=False)
    # image_template = ImageProcessing.resizeImage(image_template, (480, 640))
    # image_scene = ImageProcessing.grayScale(image_scene, filter_it=False, show_it=False)
    # image_scene = ImageProcessing.resizeImage(image_scene, (480, 640))

    #⬢⬢⬢⬢⬢➤ Find the Object
    matching = TemplateMatching()

    cv2.namedWindow("output", cv2.WINDOW_NORMAL)

    #▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇

    while node.isActive():
        image = camera.getImage()
        try:
            if image is not None:
                image_matching = matching.templateMatching(image_template, image)
                cv2.imshow("output", image_matching)
                c = cv2.waitKey(1)
        except Exception as e:
            Logger.error(e)

        node.tick()
    # plt.imshow(image_matching, 'gray'), plt.show()
