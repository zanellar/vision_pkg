#!/usr/bin/env python
# -*- encoding: utf-8 -*-


''' Template matching of a pre-loaded image'''


import rospy
import math
import numpy as np
import PyKDL
import superros.transformations as transformations
from superros.logger import Logger
from superros.comm import RosNode

# Vision
import cv2
from visionpylib.image import ImageProcessing
import visionpylib.visionutils as visionutils

# matching matching
from visionpylib.matching import TemplateMatching

from matplotlib import pyplot as plt


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_visione")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    #⬢⬢⬢⬢⬢➤ Load Image
    image_template_name = "/home/riccardo/Pictures/vision/test_template/template21.jpg"
    image_scene_name = "/home/riccardo/Pictures/vision/test_template/scene22.jpg"

    image_template = cv2.imread(image_template_name, cv2.IMREAD_COLOR)
    image_scene = cv2.imread(image_scene_name, cv2.IMREAD_COLOR)

    #⬢⬢⬢⬢⬢➤ Image Processing
    # image_template = ImageProcessing.grayScale(image_template, filter_it=False, show_it=False)
    # image_template = ImageProcessing.resizeImage(image_template, (480, 640))
    # image_scene = ImageProcessing.grayScale(image_scene, filter_it=False, show_it=False)
    # image_scene = ImageProcessing.resizeImage(image_scene, (480, 640))

    #⬢⬢⬢⬢⬢➤ Find the Object
    feature = TemplateMatching()
    image_matching = feature.templateMatching(image_template, image_scene)
    plt.imshow(image_matching, 'gray'), plt.show()
