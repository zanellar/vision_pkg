# -*- encoding: utf-8 -*-
"""Cameras management."""
import cv2
import rospy
import numpy as np
import message_filters
from sensor_msgs.msg import Image, CameraInfo, CompressedImage
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge

from superros.logger import Logger
import os
import sys


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

class Camera(object):

    def __init__(self, configuration_file):
        self.configuration_file = configuration_file

        cfg_filename, cfg_file_extension = os.path.splitext(self.configuration_file)
        raw_configuration_file = cfg_filename + ".txt"

        if not os.path.exists(self.configuration_file) or not os.path.exists(raw_configuration_file):
            Logger.error("Camera configuration file not found! '{}'".format(self.configuration_file))
            sys.exit(0)

        if os.path.exists(raw_configuration_file):
            raw = np.loadtxt(raw_configuration_file)
            self.width = int(raw[0])
            self.height = int(raw[1])
            self.fx = raw[2]
            self.fy = raw[3]
            self.cx = raw[4]
            self.cy = raw[5]
            self.k1 = raw[6]
            self.k2 = raw[7]
            self.p1 = raw[8]
            self.p2 = raw[9]
            if len(raw) > 10:
                self.k3 = raw[10]
            else:
                self.k3 = 0.0
        else:
            self.width = 640
            self.height = 480
            self.fx = 538.399364
            self.fy = 538.719401
            self.cx = 316.192117
            self.cy = 240.080174
            self.k1 = 0.047223
            self.k2 = -0.124558
            self.p1 = 0.002427
            self.p2 = -0.001539
            self.k3 = 0.0

            Logger.error("CAMERA CALIBRATION IS HARDCODED!! MISSING TXT CONFIGURATION FILE:{}".format(
                raw_configuration_file))

        self.camera_matrix = np.array([])
        self.camera_matrix_inv = np.array([])
        self.distortion_coefficients = np.array([])
        self.buildCameraMatrix()

    def buildCameraMatrix(self):
        self.camera_matrix = np.array(
            [[self.fx, 0, self.cx], [0, self.fy, self.cy], [0, 0, 1]])
        self.distortion_coefficients = np.array([
            self.k1, self.k2, self.p1, self.p2, self.k3
        ])
        self.camera_matrix_inv = np.linalg.inv(self.camera_matrix)

    def getCameraFile(self):
        return self.configuration_file

    def get3DPoint(self, u, v, framergbd):
        d = float(framergbd.depth_image[u, v]) / float(framergbd.depth_scale)
        px = (d / self.fx) * (v - self.cx)
        py = (d / self.fy) * (u - self.cy)
        pz = d
        return np.array([px, py, pz])


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class FrameRGBD(object):
    CV_BRIDGE = CvBridge()

    def __init__(self, rgb_image=None, depth_image=None, time=None):
        self.rgb_image = rgb_image
        self.depth_image = depth_image
        self.depth_scale = 1
        self.time = time
        if self.rgb_image == None or self.depth_image == None:
            self.valid = False
        else:
            self.valid = True

    def isValid(self):
        return self.valid

    def getPointCloud(self, camera, mask=None):
        points = []
        if self.isValid():
            for u in range(0, self.rgb_image.shape[0]):
                for v in range(0, self.rgb_image.shape[1]):
                    p = camera.get3DPoint(u, v, self)
                    points.append(p)
        return points

    @staticmethod
    def buildFromMessages(rgb_msg, depth_msg, depth_scale=1000):
        frame = FrameRGBD()
        frame.depth_scale = 1000
        frame.time = rgb_msg.header.stamp
        try:
            frame.rgb_image = FrameRGBD.CV_BRIDGE.imgmsg_to_cv2(
                rgb_msg, "bgr8")
        except CvBridgeError as e:
            Logger.error(e)
            return frame

        try:
            frame.depth_image = FrameRGBD.CV_BRIDGE.imgmsg_to_cv2(
                depth_msg, "16UC1")
        except CvBridgeError as e:
            Logger.error(e)
            return frame

        frame.valid = True
        return frame

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class CameraRGBD(Camera):

    def __init__(self, configuration_file, rgb_topic, depth_topic, callback_buffer_size=1, approx_time_sync=0.1):
        super(CameraRGBD, self).__init__(configuration_file=configuration_file)
        self.rgb_topic = rgb_topic
        self.depth_topic = depth_topic
        self.callback_buffer_size = callback_buffer_size
        self.approx_time_sync = approx_time_sync

        self.rgb_sub = message_filters.Subscriber(self.rgb_topic, Image)
        self.depth_sub = message_filters.Subscriber(self.depth_topic, Image)

        ts = message_filters.ApproximateTimeSynchronizer(
            [self.rgb_sub, self.depth_sub], callback_buffer_size, approx_time_sync)
        ts.registerCallback(self.topicCallback)

        self._user_callbacks = []

    def registerUserCallabck(self, callback):
        self._user_callbacks.append(callback)

    def topicCallback(self, rgb_msg, depth_msg):
        frame = FrameRGBD.buildFromMessages(
            rgb_msg, depth_msg)
        for c in self._user_callbacks:
            c(frame)


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class FrameRGB(object):
    CV_BRIDGE = CvBridge()

    def __init__(self, rgb_image=None, time=None):
        self.rgb_image = rgb_image
        self.time = time
        if self.rgb_image == None:
            self.valid = False
        else:
            self.valid = True

    def isValid(self):
        return self.valid

    @staticmethod
    def buildFromMessages(rgb_msg):
        frame = FrameRGB()
        frame.time = rgb_msg.header.stamp
        try:
            frame.rgb_image = FrameRGBD.CV_BRIDGE.imgmsg_to_cv2(
                rgb_msg, "bgr8")
        except CvBridgeError as e:
            try:
                frame.rgb_image = FrameRGBD.CV_BRIDGE.imgmsg_to_cv2(
                    rgb_msg, "8UC1")
            except CvBridgeError as e:
                Logger.error(e)
                return frame

        frame.valid = True
        return frame

    @staticmethod
    def buildFromMessagesCompressed(rgb_msg):
        frame = FrameRGB()
        frame.time = rgb_msg.header.stamp

        np_arr = np.fromstring(rgb_msg.data, np.uint8)
        try:
            frame.rgb_image = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
        except:
            try:
                frame.rgb_image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
            except:
                return frame

        frame.valid = True
        return frame

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class CameraRGB(Camera):

    def __init__(self, configuration_file, rgb_topic, callback_buffer_size=1, compressed_image=False):
        super(CameraRGB, self).__init__(configuration_file=configuration_file)
        self.rgb_topic = rgb_topic
        self.callback_buffer_size = callback_buffer_size
        self.compressed_image = compressed_image

        if compressed_image:
            self.rgb_sub = rospy.Subscriber(
                self.rgb_topic, CompressedImage, self.topicCallback, queue_size=self.callback_buffer_size)
        else:
            self.rgb_sub = rospy.Subscriber(
                self.rgb_topic, Image, self.topicCallback, queue_size=self.callback_buffer_size)
        self._user_callbacks = []

    def registerUserCallabck(self, callback):
        self._user_callbacks.append(callback)

    def topicCallback(self, rgb_msg):
        if self.compressed_image:
            frame = FrameRGB.buildFromMessagesCompressed(rgb_msg)
        else:
            frame = FrameRGB.buildFromMessages(rgb_msg)
        for c in self._user_callbacks:
            c(frame)

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class CameraHandler(object):
    def __init__(self, calibration_file, camera_topic):

        # Create sCamera Proxy
        self.camera = CameraRGB(
            configuration_file=calibration_file,
            rgb_topic=camera_topic,
            compressed_image='compressed' in camera_topic
        )

        # Camera Msgs Callback
        self.camera.registerUserCallabck(self.camera_callback)
        self.frame = None
        self.image = None

    def camera_callback(self, frame_msg):
        self.frame = frame_msg
        self.image = self.frame.rgb_image.copy()

    def getCamera(self):
        return self.camera

    def getFrame(self):
        return self.frame

    def getImage(self):
        return self.image
