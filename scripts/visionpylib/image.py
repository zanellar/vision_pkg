# -*- encoding: utf-8 -*-
""" Image Processing """

from __future__ import division, print_function
import math
import PyKDL
import rospy
import tf
import cv2
import numpy as np
from superros.logger import Logger
import superros.transformations as transformations
import visionpylib.visionutils as visionutils

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class ImageAreaOfInterest(object):
    # all the tf are 2D-frames on the marker plane
    def __init__(self, image=None, image_tf=PyKDL.Frame(), relative_tf=PyKDL.Frame()):
        self.image = image
        self.image_tf = image_tf
        self.relative_tf = relative_tf
        self.image_area = []

    def update(self, image, image_tf, relative_tf=PyKDL.Frame()):
        ''' update the image as Numpy Arrays with shape=(h,w,3) '''
        self.image = image
        self.image_tf = image_tf
        self.relative_tf = relative_tf
        self.image_area_tf = self.image_tf * self.relative_tf

    def setArea(self, height, width, base_side=0):
        ''' Define the area of interest '''
        bs = base_side
        pw = width / 2
        ph = height / 2
        self.image_area = np.array([
            [0, 0],
            [bs, ph],
            [pw, ph],
            [pw, -ph],
            [bs, -ph]
        ])
        self.image_area *= 2

    def draw(self):
        ''' Draw the area of interest and the relative reference frame on the source image'''
        visionutils.draw2DReferenceFrame(self.image_area_tf, self.image)
        relative_image_area = self._getRelativeImageArea()
        visionutils.drawPolygon(self.image, relative_image_area)

    def getMaskedImage(self, show_it=False):
        ''' Returns (and eventually shows) the image (Numpy array) with only the area of interest visible and black pixels outside it.'''
        mask = self._buildMask(self.image)
        masked = cv2.bitwise_and(self.image, self.image, mask=mask)  # TODO ???
        if show_it:
            cv2.namedWindow("masked", cv2.WINDOW_NORMAL)
            cv2.imshow("masked", masked)

        return masked

    def getCroppedArea(self, show_it=False):  # TODO work in progress
        image = self.getMaskedImage()

        angle = self.image_tf.M.GetEulerZYX()[0]
        image = ImageProcessing.rotateImage(image, angle=angle, show_it=False)
        area_coord = self._getRelativeImageArea()
        M = transformations.rot2D(angle)
        area_coord = np.array(area_coord[1:])
        # area_coord[area_coord < 0] = 0

        x1, x2, y1, y2 = 0, 0, 0, 0
        for i in range(0, image.shape[0]):
            nonzero = np.nonzero(np.array(image[i, :]))[0]
            if np.size(nonzero) > 0:
                if x1 == 0 and x2 == 0 or x2 - x1 < self.image_area[2][0]:
                    x1 = np.amin(nonzero)
                    x2 = np.amax(nonzero)
                    y1 = i
            else:
                if x1 > 0 and x2 > 0:
                    y2 = i - 1
                    break
                continue

        cropped_image = ImageProcessing.cropImage(image, x1, x2, y1, y2,  show_it=show_it)

        return image

    def _buildMask(self, target_image):
        zeros = None
        if target_image is not None:
            zeros = np.zeros((target_image.shape[0], target_image.shape[1], 1), dtype=np.int8)
            points = self._getRelativeImageArea()
            visionutils.drawPolygon(zeros, points, True, (255, 255, 255), -1)
        return zeros

    def _getRelativeImageArea(self, dtype=int):
        relative_image_area = []
        for p in self.image_area:
            pext = PyKDL.Vector(p[0], p[1], 0.0)
            pext = self.image_area_tf * pext
            relative_image_area.append(np.array([
                pext.x(),
                pext.y()
            ],  dtype=dtype))
        return relative_image_area

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class ImageProcessing(object):

    # @staticmethod
    # def templateMatch(image, template, show_it=False):
    #     # resize images
    #     image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5) # Occhiooooo imagine originale
    #     template = cv2.resize(template, (0, 0), fx=0.5, fy=0.5)

    #     # Find template
    #     result = cv2.matchTemplate(image, template, cv2.TM_CCOEFF)
    #     min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    #     top_left = max_loc
    #     h, w = template.shape
    #     bottom_right = (top_left[0] + w, top_left[1] + h)
    #     cv2.rectangle(image, top_left, bottom_right, (0, 0, 255), 4)

    #     if show_it:
    #         cv2.namedWindow("template_match", cv2.WINDOW_NORMAL)
    #         cv2.imshow("template_match", image)

    @staticmethod
    def extractPoints(image, th=200):
        points = []
        max_x = 0
        max_x_point = None
        for i in range(0, image.shape[0]):
            for j in range(0, image.shape[1]):
                if image[i, j] > th:
                    if j > max_x:
                        max_x = j
                        max_x_point = np.array([j, i])
                    points.append(np.array([j, i]))

        if len(points) <= 0:
            return [], np.array([0, 0])
        else:
            return points, max_x_point

    @staticmethod
    def grayScale(image, filter_it=False, show_it=False):
        '''Gray scale image'''
        # grasy scale conversion
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # filter
        if filter_it:
            kernel = np.ones((5, 5), np.float32) / 25
            image_gray = cv2.filter2D(image_gray, -1, kernel)

        if show_it:
            cv2.namedWindow("image_gray", cv2.WINDOW_NORMAL)
            cv2.imshow("image_gray", image_gray)

        return image_gray

    @staticmethod
    def fixedThreshold(image, th, show_it=False):
        '''Apply a fixed threshold'''
        image[image < th] = [0]
        if show_it:
            cv2.namedWindow("threshold", cv2.WINDOW_NORMAL)
            cv2.imshow("threshold", image)
        return image

    @staticmethod
    def saltAndPepper(image, th=200, kerner_size=2, show_it=False):
        '''Salt and pepper filter'''
        kernel = np.ones((kerner_size, kerner_size), np.uint8)
        cropped = cv2.erode(image, kernel)
        image_c = cv2.dilate(cropped, kernel)
        if show_it:
            cv2.namedWindow("salt_and_pepper", cv2.WINDOW_NORMAL)
            cv2.imshow("salt_and_pepper", image_c)
        return image_c

    @staticmethod
    def adaptiveThreshold(image, show_it=False):
        '''Apply an adaptive gaussian threshold'''
        # Adaptive Threshold on the image for backgorund removal
        th = cv2.adaptiveThreshold(image,
                                   255,
                                   cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                   cv2.THRESH_BINARY,
                                   11,
                                   2)
        th[th == 255] = [50]
        th[th == 0] = [255]
        if show_it:
            cv2.namedWindow("adaptive_threshold", cv2.WINDOW_NORMAL)
            cv2.imshow("adaptive_threshold", th)
        return th

    @staticmethod
    def cropImage(image, x1, x2, y1, y2, show_it=False):
        '''Crop the image'''
        try:
            crop = image[y1:y2, x1:x2]
            if show_it:
                cv2.namedWindow("crop", cv2.WINDOW_NORMAL)
                cv2.imshow("crop", crop)
            return crop
        except Exception as e:
            Logger.error(e)
            return None

    @staticmethod
    def rotateImage(image, angle, show_it=False):
        '''Rotate the image'''
        rows = image.shape[0]
        cols = image.shape[1]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle * 180 / math.pi, 1)
        dst = cv2.warpAffine(image, M, (cols, rows))
        if show_it:
            cv2.namedWindow("rotate", cv2.WINDOW_NORMAL)
            cv2.imshow("rotate", dst)
        return dst

    @staticmethod
    def resizeImage(image, d, forced=False):
        shape = image.shape[0:2]  # takes only the image dimension (not the numbers of channels)
        image_c = image
        if forced:
            x_max, y_max = d
            if shape[0] > x_max or shape[1] > y_max:
                if shape[0] > shape[1]:
                    x = x_max
                    y = y_max
                else:
                    x = y_max
                    y = x_max
                image_c = cv2.resize(image, (x, y), interpolation=cv2.INTER_CUBIC)
        else:
            ratio = max(shape) / max(d)
            if ratio > 1:
                dim_scaled = np.roll(np.array(shape) / ratio, 1)
                dim_scaled = tuple(dim_scaled.astype(int))
                image_c = cv2.resize(image, dim_scaled, interpolation=cv2.INTER_CUBIC)

        return image_c
