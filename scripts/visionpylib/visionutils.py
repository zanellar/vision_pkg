import cv2
import numpy as np
import PyKDL


def draw2DReferenceFrame(frame, image, size=-1, thick=2, tipLength=0.3, force_color=None):
    if size < 0:
        size = image.shape[0] * 0.1
    center = np.array([frame.p.x(), frame.p.y()], dtype=int)
    ax = np.array([frame.M[0, 0], frame.M[1, 0]], dtype=float) * size
    ay = np.array([frame.M[0, 1], frame.M[1, 1]], dtype=float) * size
    ax = ax.astype(int)
    ay = ay.astype(int)
    c1 = center + ax
    c2 = center + ay

    color_x = (0, 0, 255)
    color_y = (0, 255, 0)
    if force_color != None:
        color_x = color_y = force_color
    try:
        cv2.arrowedLine(image, tuple(center), tuple(c1),
                        color_x, thick, tipLength=tipLength)
        cv2.arrowedLine(image, tuple(center), tuple(c2),
                        color_y, thick, tipLength=tipLength)
    except Exception, e:
        print str(e)


def drawPolygon(image, polygon_points, is_closed=True, color=(0, 0, 255), thickness=1):
    pts = []
    for p in polygon_points:
        pts.append((int(p[0]), int(p[1])))
    if thickness > 0:
        cv2.polylines(image, np.int32([pts]), isClosed=is_closed,
                      color=color, thickness=thickness)
    else:
        cv2.fillPoly(image, np.int32([pts]), color=color)


def stackImages(images, rescale=(640, 480)):
    ready_images = []
    for img in images:
        w = img.shape[1]
        h = img.shape[0]

        ratio = float(rescale[1]) / float(h)
        w = int(w * ratio)
        h = rescale[1]  # int(h * ratio)

        new_img = img
        if len(img.shape) == 2 or img.shape[2] == 1:
            new_img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        new_img = cv2.resize(
            new_img,
            (w, h),
            interpolation=cv2.INTER_CUBIC
        )
        print("Stacking", new_img.shape)
        ready_images.append(new_img)
    return np.hstack(tuple(ready_images))

#######################################################
