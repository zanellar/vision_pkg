#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Markers detection."""

from __future__ import division, print_function
import math
import PyKDL
import rospy
import tf
import cv2
from cv2 import aruco
import numpy as np
from superros.logger import Logger
import superros.transformations as transformations
index = 0

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class ARMarker(PyKDL.Frame):

    def __init__(self, Rvec=None, Tvec=None, esqns=None, marker_id=None, z_up=False):
        super(ARMarker, self).__init__()
        self.Rvec = Rvec
        self.Tvec = Tvec[0]
        self.esqn = esqns
        self.esqns = esqns[0]
        self.marker_id = marker_id
        self.z_up = z_up
        rot, _ = cv2.Rodrigues(self.Rvec)

        self.M = PyKDL.Rotation(                    # Is rotation of the frame, pertenece a Frame (self)
            rot[0, 0], rot[0, 1], rot[0, 2],
            rot[1, 0], rot[1, 1], rot[1, 2],
            rot[2, 0], rot[2, 1], rot[2, 2]
        )
        self.p = PyKDL.Vector(                      # Traslation of the frame
            self.Tvec[[0]],
            self.Tvec[[1]],
            self.Tvec[[2]]
        )
        if self.z_up:
            self.M.DoRotX(-np.pi / 2.0)
            self.M.DoRotZ(-np.pi)

        self.corners = []
        for p in self.esqns:
            self.corners.append(p)

        self.radius = int(
            0.5 * np.linalg.norm(self.corners[0] - self.corners[2]))
        self.center = np.array([0.0, 0.0])
        for p in self.corners:
            self.center += p
        self.center = self.center / 4

        self.side_in_pixel = 0
        for i in range(0, len(self.corners)):
            i_next = (i + 1) % len(self.corners)
            p1 = np.array([self.corners[i]])
            p2 = np.array([self.corners[i_next]])
            self.side_in_pixel += np.linalg.norm(p1 - p2)
        self.side_in_pixel = self.side_in_pixel / float(len(self.corners))

    def getID(self):
        return self.marker_id[0]

    def getName(self):
        return "marker_{}".format(self.getID())

    def draw(self, image, color=np.array([255, 255, 255]), scale=1, draw_center=False):
        esqn_cnr = np.array([self.esqn])
        cv2.aruco.drawDetectedMarkers(image, esqn_cnr, self.marker_id, 1)
        if draw_center:
            cv2.circle(
                image,
                (int(self.center[0]), int(self.center[1])),
                radius=3,
                color=color,
                thickness=3 * scale
            )

    def getPlaneCoefficients(self):
        return transformations.planeCoefficientsFromFrame(self)

    def get2DFrame(self):
        ax = np.array(self.corners[1] - self.corners[0])
        ax = ax / np.linalg.norm(ax)
        ay = np.array([-ax[1], ax[0]])
        return transformations.KDLFrom2DRF(ax, ay, self.center)

    def applyCorrection(self, frame):
        self_frame = PyKDL.Frame()
        self_frame.M = self.M
        self_frame.p = self.p
        self_frame = self_frame * frame
        self.M = self_frame.M
        self.p = self_frame.p

    def measurePixelRatio(self, side_in_meter):
        return side_in_meter / self.side_in_pixel

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class CameraParameters(object):

    def __init__(self, camera_file):
        self.camparams = None
        try:
            with open(camera_file, 'r') as f:
                camparams_str = f.readlines()[0].split(" ")
                self.camparams = map(float, camparams_str)
                print(self.camparams)
        except Exception as e:
            print(e)

    def getCameraRatio(self):
        return np.array(self.camparams[0:2])

    def getCameraMatrix(self):
        v = np.array(self.camparams[2:6])
        camera_matrix = np.matrix([[v[0], 0, v[2]], [0, v[1], v[3]], [0, 0, 1.0]])
        print(camera_matrix)
        return camera_matrix

    def getCameraDistiorsion(self):
        return np.array(self.camparams[-4:])


class MarkerDetector(object):

    def __init__(self, camera_file, min_size=0.01, max_size=0.5, z_up=False):
        cameraparams = CameraParameters(camera_file)
        self.cameraMatrix = cameraparams.getCameraMatrix()
        self.distCoeffs = cameraparams.getCameraDistiorsion()
        self.z_up = z_up

    def detectMarkers(self, image, markers_metric_size=-1.0, markers_map=None):
        global index
        aruco_dict = aruco.Dictionary_get(aruco.DICT_ARUCO_ORIGINAL)
        corners, ids, rejectedImgPoints = aruco.detectMarkers(image, aruco_dict)
        print(ids)
        final_markers = []
        if markers_metric_size < 0 and markers_map is None:
            return final_markers
        elif markers_map is not None:
            for marker_id in markers_map.keys():
                try:
                    marker_id = int(marker_id)
                except Exception as e:
                    print(e)

                if ids is not None:
                    detected_ids = ids.flatten()
                    if marker_id in detected_ids:
                        for i in detected_ids:
                            if i == marker_id:
                                esquinas = corners[index]
                                esqns = np.array([esquinas])
                                marker = ids[index]
                                break
                            index = index + 1
                        index = 0
                        estim_pos_marker = aruco.estimatePoseSingleMarkers(esqns, 0.01, self.cameraMatrix, self.distCoeffs)
                        rvecs, tvecs = estim_pos_marker[0:2]
                        # BUG cv-version='3.2.0-dev': estimatePoseSingleMarkers -> "rvecs, tvecs"
                        #     cv-version='3.3.1': estimatePoseSingleMarkers -> "rvecs, tvecs, _objPoints"

                        tvec = tvecs[0]
                        esqns = esqns[0]
                        final_markers.append(ARMarker(rvecs, tvec, esqns, marker, self.z_up))
        return final_markers

    def detectMarkersMap(self, image, markers_metric_size=-1.0, markers_map=None):
        markers = self.detectMarkers(
            image, markers_metric_size, markers_map)  # Returns an array of arrays (Rotation Vector M, and Traslation Vector p)
        markers_map = {}
        for marker in markers:
            markers_map[marker.getID()] = marker
        return markers_map  # markers_map is a list of the ids detected
