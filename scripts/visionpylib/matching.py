# -*- encoding: utf-8 -*-
""" Image Processing """

from __future__ import division, print_function
import math
import PyKDL
import rospy
import tf
import cv2
import numpy as np
from superros.logger import Logger
import superros.transformations as transformations
import visionpylib.visionutils as visionutils

from vision_pkg.srv import PolyDetectionService
from cv_bridge import CvBridge, CvBridgeError

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class FeatureMatching(object):
    ELSD = 1
    HOUGH = 2

    def __init__(self):
        self.esld_service = None
        self.cvbridge = CvBridge()
        self.circles_detected = []

    def setELSDService(self, node):
        self.esld_service = node.getServiceProxy("/elsd_detector_service/detection_service", PolyDetectionService)

    def detectCircle(self, image, min_radius, max_radius, method=None, show_it=False):
        self.circles_detected = []

        if method is None:
            method = FeatureMatching.ELSD
        try:
            if method == FeatureMatching.ELSD:
                self._circleELSD(image, min_radius, max_radius)
            elif method == FeatureMatching.HOUGH:
                self._circleHough(image, min_radius, max_radius)

            if show_it:
                cv2.namedWindow("circle_detection", cv2.WINDOW_NORMAL)
                cv2.imshow("circle_detection", image)

        except Exception as e:
            Logger.error(e)

        return self.circles_detected

    def _circleELSD(self, image, min_radius, max_radius):
        detected = []
        if self.esld_service:
            image_msg = self.cvbridge.cv2_to_imgmsg(image, "8UC1")
            res = self.esld_service(colored=False, image=image_msg, circle_max_radius=max_radius)
            for c in res.circles:
                center = (int(c.x), int(c.y))
                radius = int(c.z)
                if radius > min_radius:
                    cv2.circle(image, center, radius, (255, 0, 0), thickness=3)
                    detected.append(center)
        else:
            Logger.error("ELSD Proxy NOT Found")

        self.circles_detected = detected

    def _circleHough(self, image, min_radius, max_radius):
        circles = cv2.HoughCircles(image, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=min_radius, maxRadius=max_radius)
        circles = np.uint16(np.around(circles))
        detected = []
        for i in circles[0, :]:
            center = (i[0], i[1])
            # draw the outer circle
            cv2.circle(image, center, i[2], (255, 0, 0), 2)
            # draw the center of the circle
            cv2.circle(image, center, 2, (255, 0, 0), 3)
            detected.append(center)
        self.circles_detected = detected

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class TemplateMatching(object):
    def __init__(self):
        # Initiate ORB detector
        self.orb = cv2.ORB_create()

    def templateMatching(self, image_template, image_scene):

        # find the keypoints with ORB
        kp1 = self.orb.detect(image_template, None)
        kp2 = self.orb.detect(image_scene, None)

        # compute the descriptors with ORB
        kp1, des1 = self.orb.compute(image_template, kp1)
        kp2, des2 = self.orb.compute(image_scene, kp2)

        # FlannBasedMatcher wants the descriptors in the format floats32 (i.e. CV_32F)
        des1 = np.float32(des1)
        des2 = np.float32(des2)

        FLANN_INDEX_KDTREE = 0
        # index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        index_params = dict(algorithm=1,
                            table_number=12,  # 6
                            key_size=20,     # 12
                            multi_probe_level=2)  # 1
        search_params = dict(checks=100)

        # Flann Based Matcher object
        flann = cv2.FlannBasedMatcher(index_params, search_params)

        # Match descriptors.
        matches = flann.knnMatch(des1, des2, k=2)

        # store all the good matches as per Lowe's ratio test.
        good = []
        for m, n in matches:
            if m.distance < 0.7 * n.distance:
                good.append(m)

        min_match_count = 10
        if len(good) > min_match_count:
            src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
            matchesMask = mask.ravel().tolist()

            h, w = image_template.shape[0:2]
            pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)  # coordinates of the box corners in the current view/scene

            image_scene = cv2.polylines(image_scene, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)  # draw the bounding box
            print("Matches are found - {}/{}".format(len(good), min_match_count))

        else:
            print("Not enough matches are found - {}/{}".format(len(good), min_match_count))
            matchesMask = None

        draw_params = dict(matchColor=(0, 255, 0),  # draw matches in green color
                           singlePointColor=None,
                           matchesMask=matchesMask,  # draw only inliers
                           flags=2)

        image_matching = cv2.drawMatches(image_template, kp1, image_scene, kp2, good, None, **draw_params)

        return image_matching
